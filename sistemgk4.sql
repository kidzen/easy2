-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2016 at 06:33 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistemgk4`
--

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `role`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin2', 'Administrator', 'iKvs8aBRYg8HFJxqlLvAWLIGHffhtJ46', '$2y$13$Bbmi4C1nihiA.MiFWnGJ1.500fMt9BV3qU37zRd5NyElRBJAL6Y9W', '', 'admin2@mail.com', 1, 1475793082, 1475793082),
(2, 'admin1', 'norole', 'tr9dRtKLu3p5-9YhTo8dtKvEaHs-n2tt', '$2y$13$JCaKw.OeDATAorsF5gQeDuiSseEDY/OlsJAIW0.3OJOw7bvbomkIm', NULL, 'admin1@gmail.com', 10, 1475793132, 1475793132);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
