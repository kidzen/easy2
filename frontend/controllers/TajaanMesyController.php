<?php

namespace frontend\controllers;

use Yii;
use frontend\models\TajaanMesy;
use frontend\models\TajaanMesySearch;
use frontend\models\MaklumatIndent;
use frontend\models\ButirKontrak;
//use frontend\models\DataSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Exception;
use yii\db\Expression;

/**
 * TajaanMesyController implements the CRUD actions for TajaanMesy model.
 */
class TajaanMesyController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TajaanMesy models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TajaanMesySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TajaanMesy model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TajaanMesy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new TajaanMesy();
//        $modelKontrak = new ButirKontrak();
        $modelIndent = new MaklumatIndent();
        $model->scenario = 'create';

        $bilTajaan = TajaanMesy::find()
                ->where(['like', 'created_date', date('Y-m-d')])
                ->orderBy(['id' => SORT_DESC])->one();
        $bilTajaan = isset($bilTajaan)?substr($bilTajaan->no_indent, -3):0;        
        $bilTajaan = substr($bilTajaan, -3) + 1;
        $bilTajaan = sprintf("%03d", $bilTajaan);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {

            $modelIndent->no_indent = $model->no_indent;
            $modelIndent->no_kontrak = $model->no_kontrak;
            $modelIndent->jumlah_indent = $model->harga;
            $modelIndent->jenis_indent = 'TAJAAN';

            $bakiKontrak = ButirKontrak::find()
                    ->leftJoin('maklumat_indent', ['no_kontrak' => $model->no_kontrak]);
            $kontrak = MaklumatIndent::find()
                    ->where(['jenis_indent' => 'KONTRAK',])
                    ->andWhere(['no_kontrak' => $model->no_kontrak])
                    ->sum('jumlah_indent');
            $tajaan = MaklumatIndent::find()
                    ->where(['jenis_indent' => 'TAJAAN',])
                    ->andWhere(['no_kontrak' => $model->no_kontrak])
                    ->sum('jumlah_indent');

            $bakiKontrak = $kontrak - $tajaan - $modelIndent->jumlah_indent;
            $modelIndent->baki_had_bumbumg = $bakiKontrak;

//            $bakiLama = ButirKontrak::find()
//                            ->where(['no_kontrak' => $model->no_kontrak])
//                            ->orderBy('updated_date DESC')->one();
////            $bakiLama = MaklumatIndent::find()
////                    ->where(['no_kontrak' => $model->no_kontrak])
////                    ->orderBy('updated_date DESC')->one();
//            $totalHarga = TajaanMesy::find()
//                    ->where(['no_kontrak' => $model->no_kontrak])
//                    ->sum('harga');
////            $modelIndent->baki_had_bumbumg = $bakiLama->baki_had_bumbumg - $totalHarga;
//            $modelIndent->baki_had_bumbumg = $bakiLama->had_bumbung - $totalHarga;
//
            if ($model->save() && $modelIndent->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'bilTajaan' => $bilTajaan,
            ]);
        }
    }

    /**
     * Updates an existing TajaanMesy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelIndent = $this->relatedIndent($id);


        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {

            $modelIndent->no_indent = $model->no_indent;
            $modelIndent->no_kontrak = $model->no_kontrak;
            $modelIndent->jumlah_indent = $model->harga;
            $modelIndent->jenis_indent = 'TAJAAN';

//            $bakiLama = ButirKontrak::find()
//                            ->where(['no_kontrak' => $model->no_kontrak])
//                            ->orderBy('updated_date DESC')->one();
////            $bakiLama = MaklumatIndent::find()
////                    ->where(['no_kontrak' => $model->no_kontrak])
////                    ->orderBy('updated_date DESC')->one();
//            $totalHarga = TajaanMesy::find()
//                    ->where(['no_kontrak' => $model->no_kontrak])
//                    ->sum('harga');
////            $modelIndent->baki_had_bumbumg = $bakiLama->baki_had_bumbumg - $totalHarga;
//            $modelIndent->baki_had_bumbumg = $bakiLama->had_bumbung - $totalHarga;

            $bakiKontrak = ButirKontrak::find()
                    ->leftJoin('maklumat_indent', ['no_kontrak' => $model->no_kontrak]);
            $kontrak = MaklumatIndent::find()
                    ->where(['jenis_indent' => 'KONTRAK',])
                    ->andWhere(['no_kontrak' => $model->no_kontrak])
                    ->sum('jumlah_indent');
            $tajaan = MaklumatIndent::find()
                    ->where(['jenis_indent' => 'TAJAAN',])
                    ->andWhere(['no_kontrak' => $model->no_kontrak])
                    ->sum('jumlah_indent');

            $bakiKontrak = $kontrak - $tajaan - $modelIndent->jumlah_indent;
            $modelIndent->baki_had_bumbumg = $bakiKontrak;

            if ($model->save() && $modelIndent->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TajaanMesy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if ($this->relatedIndent($id)) {
            $this->relatedIndent($id)->delete();
            $this->findModel($id)->delete();
        } else {
            $this->findModel($id)->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the TajaanMesy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TajaanMesy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = TajaanMesy::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function relatedIndent($id) {
        //plis check
        if (($model = TajaanMesy::findOne($id)) !== null) {
            $modelIndent = MaklumatIndent::find()
                    ->where(['no_indent' => $model->no_indent])
                    ->one();
            return $modelIndent;
        } else {
            throw new NotFoundHttpException('The requested data does not exist.');
        }
    }

    protected function relatedKontrak($id) {
        //plis check
        if (($model = TajaanMesy::findOne($id)) !== null) {
            $modelKontrak = ButirKontrak::find()
                    ->where(['no_kontrak' => $model->no_kontrak])
                    ->one();
            return $modelKontrak;
        } else {
            throw new NotFoundHttpException('The requested data does not exist.');
        }
    }

}
