<?php
/* @var $this yii\web\View */

//use yii\web\Application;
use yii\bootstrap\Modal;

$this->title = 'My Yii Application';

//echo yii::$app->user->id;die();
?>

<div class="site-index">
        
    <div class="jumbotron">
        <h1 style="color: #cc99ff;
            -webkit-text-fill-color: #cc99ff; /* Will override color (regardless of order) */
            -webkit-text-stroke-width: 2px;
            -webkit-text-stroke-color: #000099;">
            <strong>Sistem Pengurusan Kontraktorisasi AGSE / AGSV</strong>
            <!--<strong>SISTEM PENGURUSAN KONTRAKTORISASI AGSE / AGSV</strong>-->
        </h1>

        <h1 style="color: black;
            letter-spacing: -3px;
            -webkit-text-fill-color: white; /* Will override color (regardless of order) */
            -webkit-text-stroke-width: 2px;
            -webkit-text-stroke-color: blue;">
            <!--<strong>Pangkalan Udara Gong Kedak</strong></h1>-->
            <strong>PANGKALAN UDARA GONG KEDAK</strong></h1>

        <!--<p><a class="btn btn-lg btn-success" href="http://localhost/easy2/frontend/web/index.php?r=editor%2Findex">Mulakan</a></p>-->
<?php
Modal::begin([
    'header' => '<h2>Cara-cara Penggunaan Sistem</h2>',
    'toggleButton' => ['label' => 'MULAKAN', 'class' => 'btn btn-lg btn-success'],
]);
echo '
            <div style="text-align:left;">
                <ol>
                    <li>Isi Borang :-
                        <ul>
                            <li>Butir Kontrak</li>
                            <li>Maklumat Kenderaan</li>
                            <li>Maklumat Syarikat</li>
                            <li>Maklumat Syarikat</li>
                        </ul>
                    </li>
                    <br>
                    <li>Bina Mesyuarat Jawatan Kuasa</li>
                    <br>
                    <li>Bina Tajaan berdasarkan Mesyuarat Jawatan Kuasa</li>
                    <br>
                    <li>Jika Perlu: Pastikan integriti data yang dimasukkan melalui Maklumat Indent</li>
                </ol>
            </div>
             ';
Modal::end();
?>
    </div>

    <!--    <div class="body-content">
    
            <div class="row">
                <div class="col-lg-4">
                    <h2>Heading</h2>
    
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>
    
                    <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>
    
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>
    
                    <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Heading</h2>
    
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                        ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur.</p>
    
                    <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
                </div>
            </div>
    
        </div>-->
</div>
