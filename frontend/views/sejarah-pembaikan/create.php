<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\SejarahPembaikan */

$this->title = 'Create Sejarah Pembaikan';
$this->params['breadcrumbs'][] = ['label' => 'Sejarah Pembaikan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sejarah-pembaikan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
