<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Maklumat Syarikat';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maklumat-syarikat-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Maklumat Syarikat', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label'=>'Nama Syarikat',
                'attribute'=>'nama_syarikat',
            ],
            'kod_bidang',
            'created_date',
            'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<?php
$js = <<< JS
        $('#nama_syarikat').click(function() {
            $(".nama_syarikat").toggle(this.checked);
        });
        
JS;
$this->registerJs($js);
