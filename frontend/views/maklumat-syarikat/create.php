<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\MaklumatSyarikat */

$this->title = 'Create Maklumat Syarikat';
$this->params['breadcrumbs'][] = ['label' => 'Maklumat Syarikat', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maklumat-syarikat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
