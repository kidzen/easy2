<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\MaklumatAgsvAgse */

$this->title = 'Update Maklumat AGSV/AGSE: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Maklumat AGSV/AGSE', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="maklumat-agsv-agse-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
