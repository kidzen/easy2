<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MaklumatAgsvAgseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maklumat-agsv-agse-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_daftar') ?>

    <?= $form->field($model, 'jenis_agsv_agse') ?>
    
    <?= $form->field($model, 'url_gambar') ?>

    <?= $form->field($model, 'tarikh_masuk_khidmat') ?>

    <?= $form->field($model, 'tarikh_serah_terima') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
