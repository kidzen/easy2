<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ButirKontrak */

$this->title = 'Create Butir Kontrak';
$this->params['breadcrumbs'][] = ['label' => 'Butir Kontrak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butir-kontrak-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'bilKontrak' => $bilKontrak,
    ]) ?>

</div>
