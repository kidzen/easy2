<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\ButirKontrak */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Butir Kontrak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butir-kontrak-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_indent',
            'no_kontrak',
            'id_syarikat',
            [
                'label' => 'Nama Syarikat',
//                'attribute' => 'syarikat_nama_syarikat',
//                'value' => 'idSyarikat.nama_syarikat',
                'value' => $model->idSyarikat->nama_syarikat,
            ],
            'had_bumbung',
            'tarikh_mula',
            'tarikh_tamat_kontrak',
            'revenue_kontrak',
            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>
