<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ButirKontrakSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="butir-kontrak-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_indent') ?>

    <?= $form->field($model, 'no_kontrak') ?>

    <?= $form->field($model, 'had_bumbung') ?>

    <?= $form->field($model, 'tarikh_mula') ?>

    <?php // echo $form->field($model, 'tarikh_jv') ?>

    <?php // echo $form->field($model, 'tarikh_sst') ?>

    <?php // echo $form->field($model, 'tarikh_tt_kontrak') ?>

    <?php // echo $form->field($model, 'revenue_kontrak') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
