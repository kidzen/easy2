<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
//use kartik\widgets\ActiveForm;
//use kartik\builder\Form;
//use yii\jui\DatePicker;
//use yii\bootstrap;
//use kartik\datetime\DateTimePicker;
//use kartik\date\DatePicker;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;
use frontend\models\MaklumatIndent;
use frontend\models\MaklumatSyarikat;

//use kartik\date;
/* @var $this yii\web\View */
/* @var $model frontend\models\ButirKontrak */

/* @var $form yii\widgets\ActiveForm */
//$modelIndent = new MaklumatIndent();

?>

<div class="butir-kontrak-form">


    <?php $form = ActiveForm::begin(); ?>
    <?php // echo $form->errorSummary($model); ?> 
    
    <?= $form->field($model, 'no_indent')->textInput(['maxlength' => true,'readonly'=>true,'value'=>'PUGK-KONTRAK-'.date('ymd').'/'.$bilKontrak])?>

    <?= $form->field($model, 'no_kontrak')->textInput(['maxlength' => true,'value'=>'PK-']) ?>
    
    <?=
    $form->field($model, 'id_syarikat')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(MaklumatSyarikat::find()->all(), 'id', 'nama_syarikat'),
        'options' => ['placeholder' => 'Select a company...'],
    ])->label('Nama Syarikat');
    ?>
    
    <?php // echo $form->field($model, 'had_bumbung')->textInput(['maxlength' => true]) ?>

    <?=
    $form->field($model, 'tarikh_mula')->widget(
            DatePicker::className(), [
        'value' => date('Y-m-d H:i:s'),
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'tarikh_tamat_kontrak')->widget(
            DatePicker::className(), [
        'value' => date('Y-m-d H:i:s'),
        'template' => '{addon}{input}',
        'clientOptions' => [
            'format' => 'yyyy-mm-dd'
        ]
    ]);
    ?>


<!--    <input type="checkbox" id="serah-terima" value="Show"> Edit Estimated Delivery Date
    &nbsp;&nbsp;
    <input type="checkbox" id="revenue_kontrak" value="Show"> Edit Revenue Kontrak 
    &nbsp;&nbsp;
    <input type="checkbox" id="jumlah_indent" value="Show"> Edit Jumlah Indent
    &nbsp;&nbsp;
    <input type="checkbox" id="baki_had_bumbung" value="Show"> Edit Baki Had Bumbung
    <br><br>-->

    <!--<div class='revenue_kontrak' style='display:none;'>-->
    <?= $form->field($model, 'revenue_kontrak')->textInput() ?>
    <!--</div>-->

<!--    <div class='baki_had_bumbung' style='display:none;'>
        <?php // echo $form->field($modelIndent, 'baki_had_bumbumg')->textInput() ?>
    </div>-->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$js = <<< JS
        $('#revenue_kontrak').click(function() {
            $(".revenue_kontrak").toggle(this.checked);
        });
        $('#serah-terima').click(function() {
            $(".serah-terima").toggle(this.checked);
        });
        $('#jumlah_indent').click(function() {
            $(".jumlah_indent").toggle(this.checked);
        });
        $('#baki_had_bumbung').click(function() {
            $(".baki_had_bumbung").toggle(this.checked);
        });
JS;
$this->registerJs($js);
