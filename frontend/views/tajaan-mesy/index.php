<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TajaanMesySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tajaan Mesyuarat';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tajaan-mesy-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tajaan Mesyuarat', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_indent',
            [
                'label'=>'Tajuk Mesyuarat',
                'attribute' =>'mesy_tajuk_mesy',
                'value' =>'idMesy.tajuk_mesy',
            ],
            [
                'label'=>'No Daftar',
                'attribute' =>'kenderaan_no_daftar',
                'value' =>'idAgseAgsv.no_daftar',
            ],
            [
                'label'=>'Jenis AGSV/AGSE',
                'attribute' =>'kenderaan_jenis_kenderaan',
                'value' =>'idAgseAgsv.jenis_agsv_agse',
            ],
            [
                'label'=>'Gambar AGSV/AGSE',
//                'attribute' =>'kenderaan_gambar',
                'format'=>['image',['width'=>'auto','height'=>'100']],
                'value' =>'idAgseAgsv.url_gambar',
            ],

             'no_kontrak',
             'jenis_tajaan',
             'harga',
             'edd_serah',
             'edd_terima',
             'created_date',
             'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
