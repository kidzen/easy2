<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="maklumat-syarikat-index">

    <h1>Summary</h1>

    <hr>
    <?php
    $columns = [
        [
            'class' => 'kartik\grid\SerialColumn',
            'pageSummary' => 'Page Total',
            'order' => DynaGrid::ORDER_FIX_LEFT
        ],
//        Syarikat
        [
            'label' => 'Nama syarikat',
            'attribute' => 'syarikat_nama_syarikat',
            'value' => 'syarikat.nama_syarikat',
//            'value' => 'maklumat_syarikat.nama_syarikat',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Kod Bidang Syarikat',
            'attribute' => 'syarikat_kod_bidang',
            'value' => 'syarikat.kod_bidang',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
//        indent
//        [
//            'label' => 'Indent:Id Indent',
//            'attribute' => 'id',
////            'format' => 'raw',
//            'width'=>'36px',
//            'vAlign' => 'middle',
//        ],
//        [
//            'label' => 'Indent:Jenis Indent',
//            'attribute' => 'indent_jenis_indent',
//            'value' => 'jenis_indent',
//        ],
//        [
//            'label' => 'Indent:Created Date',
//            'attribute' => 'created_date',
//            'filterType' => GridView::FILTER_DATE,
//            'format' => 'raw',
//            'width' => '100px',
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['format' => 'yyyy-mm-dd']
//            ],
//            'visible' => false,
//        ],
//        [
//            'label' => 'Indent:Updated Date',
//            'attribute' => 'updated_date',
//            'filterType' => GridView::FILTER_DATE,
//            'format' => 'raw',
//            'width' => '100px',
//            'filterWidgetOptions' => [
//                'pluginOptions' => ['format' => 'yyyy-mm-dd']
//            ],
//            'visible' => false,
//        ],
//        mesyuarat
        [
            'label' => 'Tajuk Mesy',
            'attribute' => 'mesy_tajuk_mesy',
            'value' => 'mesy.tajuk_mesy',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Tarikh Mesy',
            'attribute' => 'mesy_tarikh_mesy',
            'value' => 'mesy.tarikh_mesy',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Ahli Mesy 1',
            'attribute' => 'mesy_ahli1',
            'value' => 'mesy.ahli1',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Ahli Mesy 2',
            'attribute' => 'mesy_ahli1',
            'value' => 'mesy.ahli2',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Ahli Mesy 3',
            'attribute' => 'mesy_ahli1',
            'value' => 'mesy.ahli3',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Siri Mesy',
            'attribute' => 'mesy_siri',
            'value' => 'mesy.siri',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
//        kenderaan
        [
            'label' => 'No Daftar',
            'attribute' => 'kenderaan_no_daftar',
            'value' => 'kenderaan.no_daftar',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Gambar Kenderaan',
            'attribute' => 'kenderaan_url_gambar',
            'value' => 'kenderaan.url_gambar',
            'vAlign' => 'middle',
            'hAlign' => 'center',
            'format'=>['image',['width'=>'auto','height'=>'100']],
        ],
        [
            'label' => 'Tarikh Masuk Khidmat',
            'attribute' => 'kenderaan_tarikh_masuk_khidmat',
            'value' => 'kenderaan.tarikh_masuk_khidmat',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Tarikh Serah Terima',
            'attribute' => 'kenderaan_tarikh_serah_terima',
            'value' => 'kenderaan.tarikh_serah_terima',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
//        tajaan
        [
            'label' => 'Tajaan:Ident',
            'attribute' => 'tajaan_no_indent',
            'value' => 'tajaan.no_indent',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'EDD Serah',
            'attribute' => 'tajaan_edd_serah',
            'value' => 'tajaan.edd_serah',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'EDD Terima',
            'attribute' => 'tajaan_edd_terima',
            'value' => 'tajaan.edd_terima',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Tajaan:Jenis Tajaan',
            'attribute' => 'tajaan_jenis_tajaan',
            'value' => 'tajaan.jenis_tajaan',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Tajaan:Harga',
            'attribute' => 'tajaan_harga',
            'value' => 'tajaan.harga',
            'vAlign' => 'middle',
            'hAlign' => 'center',
            'pageSummary' => true,
        ],
//        sejarah
//        [
//            'label' => 'Jenis Pembaikan',
//            'attribute' => 'sej_jenis_baiki',
//            'value' => 'sejarah.jenis_pembaikan',
//        ],
//        carta org
//        minit
//        kontrak
        [
            'label' => 'Kontrak:Indent',
            'attribute' => 'kontrak_no_indent',
            'value' => 'kontrak.no_indent',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'No kontrak',
            'attribute' => 'kontrak_no_kontrak',
            'value' => 'kontrak.no_kontrak',
//        'value'=>'kontrak.no_kontrak',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Kontrak:Had Bumbung',
            'attribute' => 'kontrak_had_bumbung',
            'value' => 'kontrak.had_bumbung',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
        [
            'label' => 'Tarikh Mula Kontrak',
            'attribute' => 'kontrak_tarikh_mula',
            'value' => 'kontrak.tarikh_mula',
            'vAlign' => 'middle',
            'hAlign' => 'center',
            'filterType' => GridView::FILTER_DATE,
            'format' => 'raw',
            'width' => '170px',
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
            'visible' => false,
        ],
        [
            'label' => 'Tarikh Tamat Kontrak',
            'attribute' => 'kontrak_tarikh_tamat_kontrak',
            'value' => 'kontrak.tarikh_tamat_kontrak',
            'vAlign' => 'middle',
            'hAlign' => 'center',
            'filterType' => GridView::FILTER_DATE,
            'format' => 'raw',
            'width' => '170px',
            'filterWidgetOptions' => [
                'pluginOptions' => ['format' => 'yyyy-mm-dd']
            ],
            'visible' => false,
        ],
        [
            'label' => 'Revenue Kontrak',
            'attribute' => 'kontrak_revenue_kontrak',
            'value' => 'kontrak.revenue_kontrak',
            'vAlign' => 'middle',
            'hAlign' => 'center',
        ],
            //Action
//    [
//        'class'=>'kartik\grid\ActionColumn',
//        'dropdown'=>false,
//        'urlCreator'=>function($action, $model, $key, $index) { return '#'; },
//        'viewOptions'=>['title'=>'view', 'data-toggle'=>'tooltip'],
//        'updateOptions'=>['title'=>'update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['title'=>'delete', 'data-toggle'=>'tooltip'], 
//        'order'=>DynaGrid::ORDER_FIX_RIGHT
//    ],
//    ['class'=>'kartik\grid\CheckboxColumn', 'order'=>DynaGrid::ORDER_FIX_RIGHT],
    ];
    $dynagrid = DynaGrid::begin([
                'columns' => $columns,
                'theme' => 'panel-info',
                'showPersonalize' => true,
                'storage' => 'cookie',
                'gridOptions' => [
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'showPageSummary' => true,
                    'floatHeader' => false,
                    'pjax' => true,
                    'panel' => [
                        'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-book"></i> Data Library</h3>',
                        'before' => '<div style="padding-top: 7px;"><em>* You may configure and save your filter setting</em></div>',
                        'after' => false
                    ],
                    'toolbar' => [
                        ['content' =>
                            Html::button('<i class="glyphicon glyphicon-plus"></i>', ['type' => 'button', 'title' => 'Add Book', 'class' => 'btn btn-success', 'onclick' => 'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) 
//                            . ' ' .
//                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['dynagrid-demo'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset Grid'])
                        ],
                        ['content' => '{dynagridFilter}{dynagridSort}{dynagrid}'],
                        '{export}',
                    ]
                ],
                'options' => ['id' => 'dynagrid-1'] // a unique identifier is important
    ]);
    if (substr($dynagrid->theme, 0, 6) == 'simple') {
        $dynagrid->gridOptions['panel'] = true;
//        $dynagrid->gridOptions['panel'] = false;
    }
    DynaGrid::end();
    ?>
</div>


