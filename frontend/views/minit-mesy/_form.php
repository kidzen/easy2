<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\helpers\ArrayHelper;
//use kartik\select2\Select2;
//use frontend\models\MesyJawatanKuasa;

/* @var $this yii\web\View */
/* @var $model frontend\models\MinitMesy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="minit-mesy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_mesy')->textInput() ?>

    <?= $form->field($model, 'tajuk_mesy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mula')->textInput() ?>

    <?= $form->field($model, 'habis')->textInput() ?>

    <?= $form->field($model, 'jangkamasa')->textInput() ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'updated_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
