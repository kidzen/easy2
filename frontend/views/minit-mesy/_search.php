<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MinitMesySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="minit-mesy-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_mesy') ?>

    <?= $form->field($model, 'tajuk_mesy') ?>

    <?= $form->field($model, 'mula') ?>

    <?= $form->field($model, 'habis') ?>

    <?php // echo $form->field($model, 'jangkamasa') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
