<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\MinitMesy */

$this->title = 'Create Minit Mesy';
$this->params['breadcrumbs'][] = ['label' => 'Minit Mesyuarat', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="minit-mesy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
