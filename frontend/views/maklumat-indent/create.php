<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\MaklumatIndent */

$this->title = 'Create Maklumat Indent';
$this->params['breadcrumbs'][] = ['label' => 'Maklumat Indent', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maklumat-indent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
//        'modelIndent' => $modelIndent,
    ]) ?>

</div>
