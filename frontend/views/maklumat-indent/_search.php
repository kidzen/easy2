<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MaklumatIndentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="maklumat-indent-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_indent') ?>

    <?= $form->field($model, 'no_kontrak') ?>

    <?= $form->field($model, 'jenis_indent') ?>

    <?= $form->field($model, 'edd_serah') ?>

    <?= $form->field($model, 'edd_terima') ?>

    <?php // echo $form->field($model, 'jumlah_indent') ?>

    <?php // echo $form->field($model, 'baki_had_bumbumg') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
