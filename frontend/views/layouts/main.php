<?php

use yii\helpers\Html;
use frontend\assets\AdminLteAsset;

/* @var $this \yii\web\View */
/* @var $content string */


//if (isset(Yii::$app->user->id)) {
//    $online = Yii::$app->user->identity->username;
//    $role = Yii::$app->user->identity->role;
//    $created = 'Member since ' . Yii::$app->user->identity->created_at;
//    if (Yii::$app->user->identity->active()) {
//        $activeBool = true;
//    } else {
//        $activeBool = false;
//    }
//} else {
//    $online = 'Guest';
//    $role = '';
//    $created = '';
//    $activeBool = false;
//}


if (class_exists('backend\assets\AppAsset')) {
    backend\assets\AppAsset::register($this);
} else {
    frontend\assets\AppAsset::register($this);
}

//frontend\assets\AppAsset::register($this);
//dmstr\web\AdminLteAsset::register($this);
frontend\assets\AdminLteAsset::register($this);
//AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@frontend/assets/dist');
//$directoryAsset = Yii::$app->assetManager->getPublishedUrl('');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrapper">

            <?=
            $this->render(
                    'header.php', ['directoryAsset' => $directoryAsset]
            )
            ?>

            <?=
            $this->render(
                    'left.php', ['directoryAsset' => $directoryAsset]
            )
            ?>

            <?php
            if (preg_match("/\/frontend\/web\/index.php$/", 
                    $_SERVER['REQUEST_URI'])) {
                echo $this->render(
                        'content2.php', ['content' => $content, 'directoryAsset' => $directoryAsset]
                );
            } else if (preg_match("/\/frontend\/web\/index.php\?r=site%2Findex$/", 
                    $_SERVER['REQUEST_URI'])) {
                echo $this->render(
                        'content2.php', ['content' => $content, 'directoryAsset' => $directoryAsset]
                );
            } else if (preg_match("/\/frontend\/web\/index.php\?r=site\/index$/", 
                    $_SERVER['REQUEST_URI'])) {
                echo $this->render(
                        'content2.php', ['content' => $content, 'directoryAsset' => $directoryAsset]
                );
            } else {
                echo $this->render(
                        'content.php', ['content' => $content, 'directoryAsset' => $directoryAsset]
                );
            }
            ?>

        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
