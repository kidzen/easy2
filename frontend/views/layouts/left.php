<?php

use yii\db\Query;
use yii\db\ActiveQuery;
use frontend\models\MaklumatSyarikat;
use frontend\models\TajaanMesy;
use frontend\models\MesyJawatanKuasa;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div style="text-align: center">
                <img src="<?= $directoryAsset ?>/img/icons.gif" style="width: 200px" alt="TUDM"/>
                <!--<img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" style="width: 200px" alt="TUDM"/>-->
            </div>
        </div>

        <!-- search form -->
        <!--        <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                        <input type="text" name="q" class="form-control" placeholder="Search..."/>
                        <span class="input-group-btn">
                            <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>-->
        <!-- /.search form -->
        <?php
        $siries = frontend\models\MesyJawatanKuasa::find()->asArray()->all();
        $companies = frontend\models\MaklumatSyarikat::find()->all();
        $tajaans = frontend\models\TajaanMesy::find()->joinWith('idSyarikat')->all();
        foreach ($companies as $company) {
            $nsyarikat [] = ['label' => $company["nama_syarikat"], 'icon' => 'fa fa-circle-o', 'url' => '#'];
        }
        foreach ($tajaans as $tajaan) {
            $syarikat [] = ['label' => $tajaan["idSyarikat"]["nama_syarikat"], 'icon' => 'fa fa-circle-o', 'url' => '#'];
        }
        foreach ($siries as $siri) {
            $myArray [] = ['label' => "Siri " . $siri["siri"], 'icon' => 'fa fa-circle-o', 'url' => '#'];
        }
//        var_dump($syarikat);
        ?>

        <?php
        $menuJtp = '';
        $siries = TajaanMesy::find()
                ->joinWith('idSyarikat')
                ->joinWith('idMesy')
                ->groupBy('id_mesy')
                ->orderBy('siri')
                ->all();
//                    $series = MesyJawatanKuasa::find()
//                            ->groupBy('siri')
//                            ->all();
        foreach ($siries as $siri) {
            $sirie = $siri['idMesy']['siri'];
//            echo 'url' . $sirie;
            $tblTajaan = TajaanMesy::find()
                    ->joinWith('idSyarikat')
                    ->joinWith('idMesy')
                    ->groupBy('maklumat_syarikat.nama_syarikat')
                    ->where(['id_mesy' => $siri['id_mesy']])
                    ->all();
            foreach ($tblTajaan as $value) {
                $namaSyarikat = $value['idSyarikat']['nama_syarikat'];
                $idSyarikat = $value['idSyarikat']['id'];
                $menuJtp = [
                    'label' => 'JTP',
                    'icon' => 'fa fa-share',
                    'url' => '#',
                    'items' => [
                        [
                            'label' => 'Siri' . $sirie,
                            'icon' => 'fa fa-circle-o',
                            'url' => '#',
                            'items' => [
                                ['label' => $namaSyarikat, 'icon' => 'fa fa-circle-o', 'url' => ['jtp-company/index', 'siri' => $sirie, 'ids' => $idSyarikat],],
                            ],
                        ],
                    ],
                ];
            }
        }
//        
        ?>
        <?php
        $menuSyarikat = '';
        $siries = TajaanMesy::find()
                ->joinWith('idSyarikat')
                ->joinWith('idMesy')
                ->groupBy('nama_syarikat')
                ->orderBy('nama_syarikat')
                ->all();
//                    $series = MesyJawatanKuasa::find()
//                            ->groupBy('siri')
//                            ->all();
        foreach ($siries as $siri) {
            $sirie = $siri['idSyarikat']['nama_syarikat'];
            $sirieid = $siri['idSyarikat']['id'];
//                        $sirie = $siri['siri'];
            $menuSyarikat = [
                'label' => 'Syarikat',
                'icon' => 'fa fa-share',
                'url' => '#',
                'items' => [
                    ['label' => $sirie, 'icon' => 'fa fa-circle-o', 'url' => ['maklumat-syarikat/view', 'id' => $sirieid],],
                ],
            ];
            $tblTajaan = TajaanMesy::find()
                    ->joinWith('idSyarikat')
                    ->joinWith('idMesy')
                    ->groupBy('maklumat_syarikat.nama_syarikat')
                    ->where(['id_mesy' => $siri['id_mesy']])
                    ->all();
            foreach ($tblTajaan as $value) {
                $namaSyarikat = $value['idSyarikat']['nama_syarikat'];
                $idSyarikat = $value['idSyarikat']['id'];
            }
        }
        ?>

        <?php
        $menuKenderaan = '';
        $siries = TajaanMesy::find()
                ->joinWith('idSyarikat')
                ->joinWith('idMesy')
                ->joinWith('idAgseAgsv')
                ->groupBy('no_daftar')
                ->orderBy('no_daftar')
                ->all();
//                    $series = MesyJawatanKuasa::find()
//                            ->groupBy('siri')
//                            ->all();
        foreach ($siries as $siri) {
            $sirie = $siri['idAgseAgsv']['no_daftar'];
            $sirieid = $siri['idAgseAgsv']['id'];

            $menuKenderaan = [
                'label' => 'Agsv / Agse',
                'icon' => 'fa fa-share',
                'url' => '#',
                'items' => [
                    ['label' => $sirie, 'icon' => 'fa fa-circle-o', 'url' => ['maklumat-agsv-agse/view', 'id' => $sirieid],],
                ],
            ];

            $tblTajaan = TajaanMesy::find()
                    ->joinWith('idSyarikat')
                    ->joinWith('idMesy')
                    ->groupBy('maklumat_syarikat.nama_syarikat')
                    ->where(['id_mesy' => $siri['id_mesy']])
                    ->all();
            foreach ($tblTajaan as $value) {
                $namaSyarikat = $value['idSyarikat']['nama_syarikat'];
                $idSyarikat = $value['idSyarikat']['id'];
            }
        }
        ?>

        <?php
        echo
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
//                        ['label' => 'Home', 'icon' => 'fa fa-file-code-o', 'url' => ['site/index']],
                        ['label' => 'Tajaan', 'icon' => 'fa fa-file-code-o', 'url' => ['tajaan-mesy/index']],
                        ['label' => 'Indent', 'icon' => 'fa fa-file-code-o', 'url' => ['maklumat-indent/index']],
                        [
                            'label' => 'Create',
                            'icon' => 'fa fa-file-code-o',
                            'url' => '#',
                            'items' => [
//                                ['label' => 'User', 'icon' => 'fa fa-file-code-o', 'url' => ['user/create'],],
                                ['label' => 'Butir Kontrak', 'icon' => 'fa fa-file-code-o', 'url' => ['butir-kontrak/create'],],
                                ['label' => 'Maklumat Syarikat', 'icon' => 'fa fa-dashboard', 'url' => ['maklumat-syarikat/create'],],
                                ['label' => 'Mesyuarat Jawatan Kuasa', 'icon' => 'fa fa-dashboard', 'url' => ['mesy-jawatan-kuasa/create'],],
                                ['label' => 'Maklumat AGSV/AGSE', 'icon' => 'fa fa-dashboard', 'url' => ['maklumat-agsv-agse/create'],],
//                                ['label' => 'Tajaan Mesyuarat', 'icon' => 'fa fa-dashboard', 'url' => ['tajaan-mesy/create'],],
//                                ['label' => 'Sejarah Pembaikan', 'icon' => 'fa fa-dashboard', 'url' => ['sejarah-pembaikan/create'],],
//                                ['label' => 'Maklumat Indent', 'icon' => 'fa fa-dashboard', 'url' => ['maklumat-indent/create'],],
//                                ['label' => 'Carta Organisasi Syarikat', 'icon' => 'fa fa-dashboard', 'url' => ['carta-organisasi-syarikat/create'],],
//                                ['label' => 'Minit Mesyuarat', 'icon' => 'fa fa-dashboard', 'url' => ['minit-mesy/create'],],
                            ],
                        ],
                        $menuJtp,
                        $menuSyarikat,
                        $menuKenderaan,
//                        [
//                            'label' => 'JTP',
//                            'icon' => 'fa fa-share',
//                            'url' => '#',
//                            'items' => [
//                                ['label' => 'User', 'icon' => 'fa fa-file-code-o', 'url' => ['user/index'],],
//                            ],
//                        ],
                        [
                            'label' => 'Administration',
                            'icon' => 'fa fa-share',
                            'url' => '#',
                            'items' => [
                                ['label' => 'User', 'icon' => 'fa fa-file-code-o', 'url' => ['user/index'],],
                                ['label' => 'Butir Kontrak', 'icon' => 'fa fa-file-code-o', 'url' => ['butir-kontrak/index'],],
                                ['label' => 'Maklumat Syarikat', 'icon' => 'fa fa-dashboard', 'url' => ['maklumat-syarikat/index'],],
                                ['label' => 'Mesyuarat Jawatan Kuasa', 'icon' => 'fa fa-dashboard', 'url' => ['mesy-jawatan-kuasa/index'],],
                                ['label' => 'Maklumat AGSV/AGSE', 'icon' => 'fa fa-dashboard', 'url' => ['maklumat-agsv-agse/index'],],
                                ['label' => 'Tajaan Mesyuarat', 'icon' => 'fa fa-dashboard', 'url' => ['tajaan-mesy/index'],],
                                ['label' => 'Sejarah Pembaikan', 'icon' => 'fa fa-dashboard', 'url' => ['sejarah-pembaikan/index'],],
                                ['label' => 'Maklumat Indent', 'icon' => 'fa fa-dashboard', 'url' => ['maklumat-indent/index'],],
                                ['label' => 'Carta Organisasi Syarikat', 'icon' => 'fa fa-dashboard', 'url' => ['carta-organisasi-syarikat/index'],],
                                ['label' => 'Minit Mesyuarat', 'icon' => 'fa fa-dashboard', 'url' => ['minit-mesy/index'],],
                                ['label' => 'Query Info', 'icon' => 'fa fa-dashboard', 'url' => ['editor/index'],],
                            ],
                        ],
                    ],
                ]
        )
        ?>

    </section>

</aside>
