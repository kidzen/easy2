<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\MesyJawatanKuasa */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mesy Jawatan Kuasa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mesy-jawatan-kuasa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tajuk_mesy',
            'tarikh_mesy',
            'ahli1',
            'ahli2',
            'ahli3',
            'siri',
            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>
