<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mesy Jawatan Kuasa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mesy-jawatan-kuasa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Mesy Jawatan Kuasa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tajuk_mesy',
            'tarikh_mesy',
            'ahli1',
            'ahli2',
            'ahli3',
            'siri',
             'created_date',
             'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
