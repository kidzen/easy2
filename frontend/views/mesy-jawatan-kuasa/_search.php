<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MesyJawatanKuasaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mesy-jawatan-kuasa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tajuk_mesy') ?>

    <?= $form->field($model, 'tarikh_mesy') ?>

    <?= $form->field($model, 'id_lst_ahli') ?>

    <?= $form->field($model, 'siri') ?>

    <?php // echo $form->field($model, 'created_date') ?>

    <?php // echo $form->field($model, 'updated_date') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
