<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\MesyJawatanKuasa */

$this->title = 'Update Mesy Jawatan Kuasa: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mesy Jawatan Kuasa', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mesy-jawatan-kuasa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
