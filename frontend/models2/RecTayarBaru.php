<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "rec_tayar_baru".
 *
 * @property integer $id
 * @property integer $no_tayar
 * @property string $tarikh_pasang
 * @property string $tarikh_tanggal
 */
class RecTayarBaru extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rec_tayar_baru';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_tayar'], 'integer'],
            [['tarikh_pasang', 'tarikh_tanggal'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_tayar' => 'No Tayar',
            'tarikh_pasang' => 'Tarikh Pasang',
            'tarikh_tanggal' => 'Tarikh Tanggal',
        ];
    }
}
