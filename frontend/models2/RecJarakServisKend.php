<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "rec_jarak_servis_kend".
 *
 * @property integer $id
 * @property string $tarikh
 * @property string $jarak_bulanan
 * @property string $meter
 * @property string $tarikh_servis
 * @property string $tarikh_rawatan_pasukan
 * @property string $tarikh_periksa_pasukan
 */
class RecJarakServisKend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rec_jarak_servis_kend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tarikh', 'tarikh_servis', 'tarikh_rawatan_pasukan', 'tarikh_periksa_pasukan'], 'safe'],
            [['jarak_bulanan', 'meter'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tarikh' => 'Tarikh',
            'jarak_bulanan' => 'Jarak Bulanan',
            'meter' => 'Meter',
            'tarikh_servis' => 'Tarikh Servis',
            'tarikh_rawatan_pasukan' => 'Tarikh Rawatan Pasukan',
            'tarikh_periksa_pasukan' => 'Tarikh Periksa Pasukan',
        ];
    }
}
