<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "butir_kenderaan".
 *
 * @property integer $id
 * @property string $no_daftar
 * @property string $jenis_kenderaan
 * @property string $muatan
 * @property string $kategori
 * @property string $saiz
 * @property string $pacuan_roda
 * @property string $buatan
 * @property string $mark
 * @property string $no_chasis
 * @property string $no_injin
 * @property string $no_fip
 * @property string $no_kontrak
 * @property string $tarikh_masuk
 *
 * @property BrgPeriksaKenderaan[] $brgPeriksaKenderaans
 */
class ButirKenderaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'butir_kenderaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_daftar', 'jenis_kenderaan', 'muatan', 'kategori', 'saiz', 'pacuan_roda', 'buatan', 'mark', 'no_chasis', 'no_injin', 'no_fip', 'no_kontrak', 'tarikh_masuk'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_daftar' => 'No Daftar',
            'jenis_kenderaan' => 'Jenis Kenderaan',
            'muatan' => 'Muatan',
            'kategori' => 'Kategori',
            'saiz' => 'Saiz',
            'pacuan_roda' => 'Pacuan Roda',
            'buatan' => 'Buatan',
            'mark' => 'Mark',
            'no_chasis' => 'No Chasis',
            'no_injin' => 'No Injin',
            'no_fip' => 'No Fip',
            'no_kontrak' => 'No Kontrak',
            'tarikh_masuk' => 'Tarikh Masuk',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrgPeriksaKenderaans()
    {
        return $this->hasMany(BrgPeriksaKenderaan::className(), ['id_kenderaan' => 'id']);
    }
}
