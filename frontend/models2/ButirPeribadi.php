<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "butir_peribadi".
 *
 * @property integer $id
 * @property string $no_kp
 * @property string $no_tentera
 * @property string $pkt
 * @property string $nama
 * @property string $pasukan
 * @property string $jawatan
 */
class ButirPeribadi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'butir_peribadi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_kp', 'no_tentera', 'pkt', 'nama', 'pasukan', 'jawatan'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_kp' => 'No Kp',
            'no_tentera' => 'No Tentera',
            'pkt' => 'Pkt',
            'nama' => 'Nama',
            'pasukan' => 'Pasukan',
            'jawatan' => 'Jawatan',
        ];
    }
}
