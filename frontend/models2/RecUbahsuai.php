<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "rec_ubahsuai".
 *
 * @property integer $id
 * @property string $id_rujukan
 * @property string $tajuk_ubahsuai
 * @property string $tarikh_keluar
 * @property string $keutamaan
 * @property string $tarikh_siap
 * @property string $meter_siap
 * @property string $approval
 */
class RecUbahsuai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rec_ubahsuai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tarikh_keluar', 'tarikh_siap'], 'safe'],
            [['id_rujukan', 'keutamaan', 'meter_siap', 'approval'], 'string', 'max' => 50],
            [['tajuk_ubahsuai'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_rujukan' => 'Id Rujukan',
            'tajuk_ubahsuai' => 'Tajuk Ubahsuai',
            'tarikh_keluar' => 'Tarikh Keluar',
            'keutamaan' => 'Keutamaan',
            'tarikh_siap' => 'Tarikh Siap',
            'meter_siap' => 'Meter Siap',
            'approval' => 'Approval',
        ];
    }
}
