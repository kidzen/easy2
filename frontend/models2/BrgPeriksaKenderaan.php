<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "brg_periksa_kenderaan".
 *
 * @property integer $id
 * @property integer $id_kenderaan
 * @property string $meter
 * @property string $tarikh
 * @property string $peralatan
 * @property string $tindakan
 * @property integer $kuantiti
 * @property string $status_serah
 * @property string $status_terima
 * @property integer $id_serah
 * @property integer $id_terima
 * @property string $tarikh_serah
 * @property string $tarikh_terima
 *
 * @property ButirKenderaan $idKenderaan
 */
class BrgPeriksaKenderaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brg_periksa_kenderaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kenderaan', 'kuantiti', 'id_serah', 'id_terima'], 'integer'],
            [['tarikh', 'tarikh_serah', 'tarikh_terima'], 'safe'],
            [['meter', 'peralatan'], 'string', 'max' => 50],
            [['tindakan', 'status_serah', 'status_terima'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kenderaan' => 'Id Kenderaan',
            'meter' => 'Meter',
            'tarikh' => 'Tarikh',
            'peralatan' => 'Peralatan',
            'tindakan' => 'Tindakan',
            'kuantiti' => 'Kuantiti',
            'status_serah' => 'Status Serah',
            'status_terima' => 'Status Terima',
            'id_serah' => 'Id Serah',
            'id_terima' => 'Id Terima',
            'tarikh_serah' => 'Tarikh Serah',
            'tarikh_terima' => 'Tarikh Terima',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKenderaan()
    {
        return $this->hasOne(ButirKenderaan::className(), ['id' => 'id_kenderaan']);
    }
}
