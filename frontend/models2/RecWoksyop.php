<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "rec_woksyop".
 *
 * @property integer $id
 * @property string $tarikh_masuk_woksyop
 * @property double $meter
 * @property string $keterangan
 * @property string $klasifikasi
 * @property string $id_woksyop
 */
class RecWoksyop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rec_woksyop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tarikh_masuk_woksyop'], 'safe'],
            [['meter'], 'number'],
            [['keterangan', 'klasifikasi', 'id_woksyop'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tarikh_masuk_woksyop' => 'Tarikh Masuk Woksyop',
            'meter' => 'Meter',
            'keterangan' => 'Keterangan',
            'klasifikasi' => 'Klasifikasi',
            'id_woksyop' => 'Id Woksyop',
        ];
    }
}
