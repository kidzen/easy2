<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "butir_pemandu".
 *
 * @property integer $id
 * @property integer $no_tentera
 * @property string $pkt
 * @property string $nama
 * @property string $pasukan
 */
class ButirPemandu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'butir_pemandu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_tentera'], 'integer'],
            [['pkt', 'nama', 'pasukan'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_tentera' => 'No Tentera',
            'pkt' => 'Pkt',
            'nama' => 'Nama',
            'pasukan' => 'Pasukan',
        ];
    }
}
