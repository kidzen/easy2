<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "rec_kend_tukar_pasukan".
 *
 * @property integer $id
 * @property string $no_kenderaan
 * @property string $jenis_kenderaan
 * @property string $tukar_caw
 */
class RecKendTukarPasukan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rec_kend_tukar_pasukan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_kenderaan', 'jenis_kenderaan'], 'string', 'max' => 50],
            [['tukar_caw'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_kenderaan' => 'No Kenderaan',
            'jenis_kenderaan' => 'Jenis Kenderaan',
            'tukar_caw' => 'Tukar Caw',
        ];
    }
}
