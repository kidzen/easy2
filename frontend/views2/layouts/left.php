<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                        ['label' => 'Home', 'icon' => 'fa fa-file-code-o', 'url' => ['site/index']],
//                    ['label' => 'Contact', 'icon' => 'fa fa-dashboard', 'url' => ['site/contact']],
                        ['label' => 'Pages', 'icon' => 'fa fa-dashboard', 'url' => ['pages/index']],
                        ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                        [
                            'label' => 'Database',
                            'icon' => 'fa fa-share',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => 'Seksyen 1',
                                    'icon' => 'fa fa-circle-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Butir Kenderaan', 'icon' => 'fa fa-file-code-o', 'url' => ['butir-kenderaan/index'],],
                                    ],
                                ],
                                [
                                    'label' => 'Seksyen 2',
                                    'icon' => 'fa fa-circle-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Borang Periksa Kenderaan', 'icon' => 'fa fa-file-code-o', 'url' => ['brg-periksa-kenderaan/index'],],
                                        ['label' => 'Rekod Jarak, Servis Kenderaan', 'icon' => 'fa fa-file-code-o', 'url' => ['rec-jarak-servis-kend/index'],],
                                        ['label' => 'Rekod Batu Perjalanan', 'icon' => 'fa fa-file-code-o', 'url' => ['brg-perisa-kenderaan/index'],],
                                        ['label' => 'Rekod Servis', 'icon' => 'fa fa-file-code-o', 'url' => ['brg-perika-kenderaan/index'],],
                                    ],
                                ],
                                [
                                    'label' => 'Seksyen 3',
                                    'icon' => 'fa fa-circle-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Rekod Tayar Baru', 'icon' => 'fa fa-file-code-o', 'url' => ['rec-tayar-baru/index'],],
                                    ],
                                ],
                                [
                                    'label' => 'Seksyen 4',
                                    'icon' => 'fa fa-circle-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Rekod Kenderaan Tukar Pasukan', 'icon' => 'fa fa-file-code-o', 'url' => ['rec-kend-tukar-pasukan/index'],],
                                        [
                                            'label' => 'Seksyen 4A',
                                            'icon' => 'fa fa-file-code-o',
                                            'url' => '#',
                                            'items' => [
                                                ['label' => 'Butir Pemandu', 'icon' => 'fa fa-file-code-o', 'url' => ['butir-pemandu/index'],],
                                            ],
                                        ],
                                    ],
                                ],
                                [
                                    'label' => 'Seksyen 5',
                                    'icon' => 'fa fa-circle-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Rekod Pemeriksaan Teknikal', 'icon' => 'fa fa-file-code-o', 'url' => ['brg-peiksa-kenderaan/index'],],
                                        ['label' => 'Rekod Pembaikian Woksyop', 'icon' => 'fa fa-file-code-o', 'url' => ['rec-woksyop/index'],],
                                        ['label' => 'Rekod Baikpulih', 'icon' => 'fa fa-file-code-o', 'url' => ['brg-eriksa-kenderaan/index'],],
                                    ],
                                ],
                                [
                                    'label' => 'Seksyen 6',
                                    'icon' => 'fa fa-circle-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Rekod Ubahsuai', 'icon' => 'fa fa-file-code-o', 'url' => ['rec-ubahsuai/index'],],
                                    ],
                                ],
                                [
                                    'label' => 'Seksyen 7',
                                    'icon' => 'fa fa-circle-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Butir Pemandu', 'icon' => 'fa fa-file-code-o', 'url' => ['butir-pemandu/index'],],
                                        ['label' => 'Butir Peribadi', 'icon' => 'fa fa-file-code-o', 'url' => ['butir-peribadi/index'],],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'label' => 'Same tools',
                            'icon' => 'fa fa-share',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                                ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                                [
                                    'label' => 'Level One',
                                    'icon' => 'fa fa-circle-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                        [
                                            'label' => 'Level Two',
                                            'icon' => 'fa fa-circle-o',
                                            'url' => '#',
                                            'items' => [
                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                                ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]
        )
        ?>

    </section>

</aside>
