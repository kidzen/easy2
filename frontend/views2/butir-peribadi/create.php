<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ButirPeribadi */

$this->title = 'Create Butir Peribadi';
$this->params['breadcrumbs'][] = ['label' => 'Butir Peribadis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butir-peribadi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
