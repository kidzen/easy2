<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Butir Peribadis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butir-peribadi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Butir Peribadi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_kp',
            'no_tentera',
            'pkt',
            'nama',
            // 'pasukan',
            // 'jawatan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
