<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\ButirPeribadi */

$this->title = 'Update Butir Peribadi: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Butir Peribadis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="butir-peribadi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
