<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\RecWoksyop */

$this->title = 'Create Rec Woksyop';
$this->params['breadcrumbs'][] = ['label' => 'Rec Woksyops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-woksyop-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
