<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rec Woksyops';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-woksyop-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Rec Woksyop', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tarikh_masuk_woksyop',
            'meter',
            'keterangan',
            'klasifikasi',
            // 'id_woksyop',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
