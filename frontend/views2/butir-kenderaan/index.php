<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Butir Kenderaans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butir-kenderaan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Butir Kenderaan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_daftar',
            'jenis_kenderaan',
            'muatan',
            'kategori',
            // 'saiz',
            // 'pacuan_roda',
            // 'buatan',
            // 'mark',
            // 'no_chasis',
            // 'no_injin',
            // 'no_fip',
            // 'no_kontrak',
            // 'tarikh_masuk',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
