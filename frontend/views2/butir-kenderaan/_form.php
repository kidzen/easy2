<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ButirKenderaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="butir-kenderaan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_daftar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis_kenderaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'muatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kategori')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'saiz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pacuan_roda')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'buatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mark')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_chasis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_injin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_fip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'no_kontrak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tarikh_masuk')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
