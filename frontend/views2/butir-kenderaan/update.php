<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\ButirKenderaan */

$this->title = 'Update Butir Kenderaan: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Butir Kenderaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="butir-kenderaan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
