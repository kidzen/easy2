<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\ButirKenderaan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Butir Kenderaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butir-kenderaan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_daftar',
            'jenis_kenderaan',
            'muatan',
            'kategori',
            'saiz',
            'pacuan_roda',
            'buatan',
            'mark',
            'no_chasis',
            'no_injin',
            'no_fip',
            'no_kontrak',
            'tarikh_masuk',
        ],
    ]) ?>

</div>
