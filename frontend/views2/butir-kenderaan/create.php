<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ButirKenderaan */

$this->title = 'Create Butir Kenderaan';
$this->params['breadcrumbs'][] = ['label' => 'Butir Kenderaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butir-kenderaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
