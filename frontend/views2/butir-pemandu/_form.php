<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ButirPemandu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="butir-pemandu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_tentera')->textInput() ?>

    <?= $form->field($model, 'pkt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pasukan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
