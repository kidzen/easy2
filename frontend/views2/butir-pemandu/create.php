<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ButirPemandu */

$this->title = 'Create Butir Pemandu';
$this->params['breadcrumbs'][] = ['label' => 'Butir Pemandus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="butir-pemandu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
