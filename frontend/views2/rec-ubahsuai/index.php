<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rec Ubahsuais';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-ubahsuai-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Rec Ubahsuai', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_rujukan',
            'tajuk_ubahsuai',
            'tarikh_keluar',
            'keutamaan',
            // 'tarikh_siap',
            // 'meter_siap',
            // 'approval',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
