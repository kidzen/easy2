<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecUbahsuai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rec-ubahsuai-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_rujukan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tajuk_ubahsuai')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tarikh_keluar')->textInput() ?>

    <?= $form->field($model, 'keutamaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tarikh_siap')->textInput() ?>

    <?= $form->field($model, 'meter_siap')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'approval')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
