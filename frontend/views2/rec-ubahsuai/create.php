<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\RecUbahsuai */

$this->title = 'Create Rec Ubahsuai';
$this->params['breadcrumbs'][] = ['label' => 'Rec Ubahsuais', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-ubahsuai-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
