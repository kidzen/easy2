<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecUbahsuai */

$this->title = 'Update Rec Ubahsuai: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rec Ubahsuais', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rec-ubahsuai-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
