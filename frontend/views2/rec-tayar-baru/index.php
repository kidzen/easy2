<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rec Tayar Barus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-tayar-baru-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Rec Tayar Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_tayar',
            'tarikh_pasang',
            'tarikh_tanggal',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
