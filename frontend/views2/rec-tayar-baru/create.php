<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\RecTayarBaru */

$this->title = 'Create Rec Tayar Baru';
$this->params['breadcrumbs'][] = ['label' => 'Rec Tayar Barus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-tayar-baru-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
