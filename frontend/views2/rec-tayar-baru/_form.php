<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecTayarBaru */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rec-tayar-baru-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_tayar')->textInput() ?>

    <?= $form->field($model, 'tarikh_pasang')->textInput() ?>

    <?= $form->field($model, 'tarikh_tanggal')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
