<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecTayarBaru */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rec Tayar Barus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-tayar-baru-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_tayar',
            'tarikh_pasang',
            'tarikh_tanggal',
        ],
    ]) ?>

</div>
