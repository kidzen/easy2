<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\BrgPeriksaKenderaan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Brg Periksa Kenderaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brg-periksa-kenderaan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_kenderaan',
            'meter',
            'tarikh',
            'peralatan',
            'tindakan',
            'kuantiti',
            'status_serah',
            'status_terima',
            'id_serah',
            'id_terima',
            'tarikh_serah',
            'tarikh_terima',
        ],
    ]) ?>

</div>
