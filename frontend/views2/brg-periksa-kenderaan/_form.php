<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\BrgPeriksaKenderaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brg-periksa-kenderaan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_kenderaan')->textInput() ?>

    <?= $form->field($model, 'meter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tarikh')->textInput() ?>

    <?= $form->field($model, 'peralatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tindakan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kuantiti')->textInput() ?>

    <?= $form->field($model, 'status_serah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_terima')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_serah')->textInput() ?>

    <?= $form->field($model, 'id_terima')->textInput() ?>

    <?= $form->field($model, 'tarikh_serah')->textInput() ?>

    <?= $form->field($model, 'tarikh_terima')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
