<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Brg Periksa Kenderaans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brg-periksa-kenderaan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Brg Periksa Kenderaan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_kenderaan',
            'meter',
            'tarikh',
            'peralatan',
            // 'tindakan',
            // 'kuantiti',
            // 'status_serah',
            // 'status_terima',
            // 'id_serah',
            // 'id_terima',
            // 'tarikh_serah',
            // 'tarikh_terima',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
