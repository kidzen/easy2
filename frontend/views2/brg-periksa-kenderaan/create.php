<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\BrgPeriksaKenderaan */

$this->title = 'Create Brg Periksa Kenderaan';
$this->params['breadcrumbs'][] = ['label' => 'Brg Periksa Kenderaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brg-periksa-kenderaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
