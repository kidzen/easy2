<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecJarakServisKend */

$this->title = 'Update Rec Jarak Servis Kend: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rec Jarak Servis Kends', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rec-jarak-servis-kend-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
