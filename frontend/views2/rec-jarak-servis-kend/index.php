<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rec Jarak Servis Kends';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-jarak-servis-kend-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Rec Jarak Servis Kend', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tarikh',
            'jarak_bulanan',
            'meter',
            'tarikh_servis',
            // 'tarikh_rawatan_pasukan',
            // 'tarikh_periksa_pasukan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
