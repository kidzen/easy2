<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecJarakServisKend */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rec-jarak-servis-kend-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tarikh')->textInput() ?>

    <?= $form->field($model, 'jarak_bulanan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meter')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tarikh_servis')->textInput() ?>

    <?= $form->field($model, 'tarikh_rawatan_pasukan')->textInput() ?>

    <?= $form->field($model, 'tarikh_periksa_pasukan')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
