<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\RecJarakServisKend */

$this->title = 'Create Rec Jarak Servis Kend';
$this->params['breadcrumbs'][] = ['label' => 'Rec Jarak Servis Kends', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-jarak-servis-kend-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
