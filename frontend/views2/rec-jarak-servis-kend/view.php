<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecJarakServisKend */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rec Jarak Servis Kends', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-jarak-servis-kend-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tarikh',
            'jarak_bulanan',
            'meter',
            'tarikh_servis',
            'tarikh_rawatan_pasukan',
            'tarikh_periksa_pasukan',
        ],
    ]) ?>

</div>
