<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\RecKendTukarPasukan */

$this->title = 'Create Rec Kend Tukar Pasukan';
$this->params['breadcrumbs'][] = ['label' => 'Rec Kend Tukar Pasukans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-kend-tukar-pasukan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
