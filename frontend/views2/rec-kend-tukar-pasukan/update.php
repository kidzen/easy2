<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecKendTukarPasukan */

$this->title = 'Update Rec Kend Tukar Pasukan: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rec Kend Tukar Pasukans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rec-kend-tukar-pasukan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
