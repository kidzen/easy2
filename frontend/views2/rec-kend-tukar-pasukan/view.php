<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecKendTukarPasukan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rec Kend Tukar Pasukans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-kend-tukar-pasukan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'no_kenderaan',
            'jenis_kenderaan',
            'tukar_caw',
        ],
    ]) ?>

</div>
