<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\RecKendTukarPasukan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rec-kend-tukar-pasukan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_kenderaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis_kenderaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tukar_caw')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
