<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rec Kend Tukar Pasukans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rec-kend-tukar-pasukan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Rec Kend Tukar Pasukan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'no_kenderaan',
            'jenis_kenderaan',
            'tukar_caw',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
