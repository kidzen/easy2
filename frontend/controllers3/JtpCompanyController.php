<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ButirKontrak;
use frontend\models\MaklumatIndent;
use frontend\models\MaklumatAgsvAgse;
use frontend\models\MaklumatSyarikat;
use frontend\models\CartaOrganisasiSyarikat;
use frontend\models\MesyJawatanKuasa;
use frontend\models\MinitMesy;
use frontend\models\SejarahPembaikan;
use frontend\models\TajaanMesy;
use frontend\models\TajaanMesySearch;
use frontend\models\JtpSearch;
use frontend\models\AllSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Exception;

class JtpCompanyController extends Controller
{
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function actionIndex($siri,$ids)
    {
//        $param->syarikat_nama_syarikat = 'Company A';
//        $param = '';
        $searchModel = new JtpSearch();
        $searchModel->mesy_siri = $siri;
        $searchModel->syarikat_id = $ids; 
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider = $searchModel->search($data);
//        if (isset(Yii::$app->request->queryParams)){
//            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);    
//        } else{
//            $dataProvider = $searchModel->search($data);
//        }
        
        
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

}
