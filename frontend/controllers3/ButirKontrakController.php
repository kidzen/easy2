<?php

namespace frontend\controllers;

use Yii;
use frontend\models\ButirKontrak;
use frontend\models\ButirKontrakSearch;
use frontend\models\MaklumatIndent;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Exception;

/**
 * ButirKontrakController implements the CRUD actions for ButirKontrak model.
 */
class ButirKontrakController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ButirKontrak models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new ButirKontrakSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ButirKontrak model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ButirKontrak model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {

        $model = new ButirKontrak();
//        $modelIndent = new MaklumatIndent();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//        if ($model->load(Yii::$app->request->post()) && $modelIndent->load(Yii::$app->request->post())) {
//            $modelIndent->no_indent = $model->id_indent;
//            $modelIndent->had_bumbung = $modelIndent->baki_had_bumbung;
//            $modelIndent->jumlah_indent = 0;
//            $modelIndent->jenis_indent = 'Butir Kontrak';
//            $bakiLama = ButirKontrak::find()->orderBy('created_date DESC')->one();
//            $modelIndent->baki_had_bumbumg = $bakiLama;

            if ($model->save()) {
//            if ($model->save() && $modelIndent->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
//            else {
//                throw new NotFoundHttpException('The requested page does not exist.');
//            }
        } else {
            return $this->render('create', [
                        'model' => $model,
//                        'modelIndent' => $modelIndent,
            ]);
        }
    }

    /**
     * Updates an existing ButirKontrak model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ButirKontrak model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ButirKontrak model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ButirKontrak the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = ButirKontrak::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
