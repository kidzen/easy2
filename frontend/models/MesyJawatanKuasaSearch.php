<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\MesyJawatanKuasa;

/**
 * MesyJawatanKuasaSearch represents the model behind the search form about `frontend\models\MesyJawatanKuasa`.
 */
class MesyJawatanKuasaSearch extends MesyJawatanKuasa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['tajuk_mesy', 'tarikh_mesy', 'ahli1', 'ahli2', 'ahli3', 'siri', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MesyJawatanKuasa::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tarikh_mesy' => $this->tarikh_mesy,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'tajuk_mesy', $this->tajuk_mesy])
            ->andFilterWhere(['like', 'ahli1', $this->ahli1])
            ->andFilterWhere(['like', 'ahli2', $this->ahli2])
            ->andFilterWhere(['like', 'ahli3', $this->ahli3])
            ->andFilterWhere(['like', 'siri', $this->siri]);

        return $dataProvider;
    }
}
