<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "tajaan_mesy".
 *
 * @property integer $id
 * @property string $no_indent 
 * @property integer $id_mesy
 * @property integer $id_syarikat
 * @property integer $id_agse_agsv
 * @property string $no_kontrak
 * @property string $jenis_tajaan
 * @property double $harga
 * @property string $edd_serah 
 * @property string $edd_terima 
 * @property string $created_date
 * @property string $updated_date
 *
 * @property MesyJawatanKuasa $idMesy
 * @property MaklumatSyarikat $idSyarikat
 * @property MaklumatAgsvAgse $idAgseAgsv 
 * @property ButirKontrak $noKontrak 
 */
class TajaanMesy extends ActiveRecord {

    public function behaviors() {
        return [

//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_date',
//                'updatedAtAttribute' => 'updated_date',
//                'value' => date('Y-m-d H:i:s'),
//            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_date', 'updated_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_date'],
                ],
//                     'value' => date('Y-m-d H:i:s'),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'tajaan_mesy';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_mesy', 'id_syarikat', 'id_agse_agsv'], 'integer'],
            [['harga'], 'number'],
            [['edd_serah', 'edd_terima', 'created_date', 'updated_date'], 'safe'],
            [['no_indent', 'no_kontrak', 'jenis_tajaan'], 'string', 'max' => 50],
            ['no_indent', 'unique','on' => 'create', 'targetClass' => '\frontend\models\MaklumatIndent', 
                'message' => 'This indent no has already exist.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'no_indent' => 'No Resit',
            'id_mesy' => 'Id Mesy',
            'id_agse_agsv' => 'Id AGSE AGSV',
            'no_kontrak' => 'No Kontrak',
            'jenis_tajaan' => 'Jenis Tajaan',
            'harga' => 'Harga',
            'edd_serah' => 'Edd Serah',
            'edd_terima' => 'Edd Terima',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMesy() {
        return $this->hasOne(MesyJawatanKuasa::className(), ['id' => 'id_mesy']);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getIdAgseAgsv() {
        return $this->hasOne(MaklumatAgsvAgse::className(), ['id' => 'id_agse_agsv']);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getNoKontrak() {
        return $this->hasOne(ButirKontrak::className(), ['no_kontrak' => 'no_kontrak']);
    }
    
    public function getIdSyarikat() {
        return $this->hasOne(MaklumatSyarikat::className(), ['id' => 'id_syarikat'])
                ->via('noKontrak');
    }
//    public function getNoIndent() {
//        return $this->hasMany(NoKontrak::className(), ['no_indent' => 'noKontrak.no_indent']);
////        return $this->hasMany(MaklumatIndent::className(), ['no_indent' => 'no_indent'])
////                ->viaTable('butir_kontrak',['no_kontrak' => 'no_kontrak']);
////        return $this->hasMany(MaklumatIndent::className(), ['no_indent' => 'no_indent'])
////                ->via('noKontrak');
////        return $this->hasMany(ButirKontrak::className(), ['no_kontrak' => 'no_kontrak'])
////                ->from(ButirKontrak::tableName())
////                ->viaTable(MaklumatIndent::tableName(),['no_indent' => 'no_indent']);
////        return $this->hasMany(MaklumatIndent::className(), ['no_indent' => 'no_indent'])
////                ->from(MaklumatIndent::tableName())
////                ->viaTable(ButirKontrak::tableName(),['no_kontrak' => 'no_kontrak']);
//    }

}
