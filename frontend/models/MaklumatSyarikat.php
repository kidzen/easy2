<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "maklumat_syarikat".
 *
 * @property integer $id
 * @property string $nama_syarikat
 * @property string $kod_bidang
 * @property string $created_date
 * @property string $updated_date
 *
 * @property CartaOrganisasiSyarikat[] $cartaOrganisasiSyarikats
 * @property TajaanMesy[] $tajaanMesies
 */
class MaklumatSyarikat extends \yii\db\ActiveRecord
{
    public function behaviors() {
        return [
            
//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_date',
//                'updatedAtAttribute' => 'updated_date',
//                'value' => date('Y-m-d H:i:s'),
//            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_date', 'updated_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_date'],
                ],
//                     'value' => date('Y-m-d H:i:s'),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'maklumat_syarikat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_date', 'updated_date'], 'safe'],
            [['nama_syarikat', 'kod_bidang'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_syarikat' => 'Nama Syarikat',
            'kod_bidang' => 'Kod Bidang',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartaOrganisasiSyarikats()
    {
        return $this->hasMany(CartaOrganisasiSyarikat::className(), ['id_syarikat' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTajaanMesies()
    {
        return $this->hasMany(TajaanMesy::className(), ['id_syarikat' => 'id']);
    }
}
