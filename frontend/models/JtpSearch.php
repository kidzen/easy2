<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\MaklumatIndent;

/**
 * TajaanMesySearch represents the model behind the search form about `frontend\models\TajaanMesy`.
 */
class JtpSearch extends MaklumatIndent {

//    public $idMesy_tajuk;
//    public $idAgseAgsv_no_daftar;
//    public $idMesy_siri;
    //Butir Kontrak
    public $kontrak_no_indent;
    public $kontrak_no_kontrak;
    public $kontrak_had_bumbung;
    public $kontrak_tarikh_mula;
    public $kontrak_tarikh_tamat_kontrak;
    public $kontrak_revenue_kontrak;
    //Mesy Jawatan Kuasa
    public $mesy_id;
    public $mesy_tajuk_mesy;
    public $mesy_tarikh_mesy;
    public $mesy_id_lst_ahli;
    public $mesy_siri;
    //Syarikat
    public $syarikat_id;
    public $syarikat_nama_syarikat;
    public $syarikat_kod_bidang;
    //kenderaan
    public $kenderaan_id;
    public $kenderaan_no_daftar;
    public $kenderaan_url_gambar;
    public $kenderaan_tarikh_masuk_khidmat;
    public $kenderaan_tarikh_serah_terima;
//    tajaan
    public $tajaan_id;
    public $tajaan_id_syarikat;
    public $tajaan_id_mesy;
    public $tajaan_no_kontrak;
    public $tajaan_jenis_tajaan;
    public $tajaan_created_date;
    public $tajaan_updated_date;
    public $tajaan_id_agse_agsv;
    public $tajaan_harga;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            //tajaan
            [['tajaan_id', 'tajaan_id_syarikat'], 'integer'],
            [['tajaan_id_mesy', 'tajaan_no_kontrak', 'tajaan_jenis_tajaan',
            'tajaan_created_date', 'tajaan_updated_date', 'tajaan_id_agse_agsv'], 'safe'],
            [['tajaan_harga'], 'number'],
//            kontrak
            [['kontrak_no_indent', 'kontrak_no_kontrak', 'kontrak_had_bumbung',
            'kontrak_tarikh_mula', 'kontrak_tarikh_tamat_kontrak',
            'kontrak_revenue_kontrak',], 'safe'],
            //Syarikat
            [['syarikat_id',  'syarikat_kod_bidang'], 'safe'],
            //Mesy
            [[ 'mesy_id_lst_ahli', 'mesy_tarikh_mesy', 'mesy_tajuk_mesy'], 'safe'],
            //Kenderaan
            [['kenderaan_no_daftar', 'kenderaan_url_gambar', 'kenderaan_tarikh_masuk_khidmat',
            'kenderaan_tarikh_serah_terima'], 'safe'],
            //indent
            [['id'], 'integer'],
            [['no_indent', 'jenis_indent', 'created_date', 'updated_date'], 'safe'],
            [['jumlah_indent', 'baki_had_bumbumg'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {

//        $query = MaklumatIndent::find();
        $query = MaklumatIndent::find();
//                $query->select('*');
//                $query->joinWith('tajaan');
                $query->joinWith('syarikat');
                $query->joinWith('sejarah');
                $query->joinWith('kontrak');
                $query->joinWith('mesy');
//                $query->joinWith('minit');
                $query->joinWith('kenderaan');
//                $query->all();
//        $query->joinWith('noIndent');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        //Sejarah
        $dataProvider->sort->attributes['sej_jenis_baiki'] = [
            'asc' => ['sejarah_pembaikan.jenis_pembaikan' => SORT_ASC],
            'desc' => ['sejarah_pembaikan.jenis_pembaikan' => SORT_DESC],
        ];
        //indent
        $dataProvider->sort->attributes['indent_no_indent'] = [
            'asc' => ['maklumat_indent.no_indent' => SORT_ASC],
            'desc' => ['maklumat_indent.no_indent' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['indent_jenis_indent'] = [
            'asc' => ['maklumat_indent.jenis_indent' => SORT_ASC],
            'desc' => ['maklumat_indent.jenis_indent' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['indent_jumlah_indent'] = [
            'asc' => ['maklumat_indent.jumlah_indent' => SORT_ASC],
            'desc' => ['maklumat_indent.jumlah_indent' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['indent_baki_had_bumbung'] = [
            'asc' => ['maklumat_indent.baki_had_bumbung' => SORT_ASC],
            'desc' => ['maklumat_indent.baki_had_bumbung' => SORT_DESC],
        ];
//        tajaan
        $dataProvider->sort->attributes['tajaan_id'] = [
            'asc' => ['tajaan_mesy.id' => SORT_ASC],
            'desc' => ['tajaan_mesy.id' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['tajaan_no_indent'] = [
            'asc' => ['tajaan_mesy.no_indent' => SORT_ASC],
            'desc' => ['tajaan_mesy.no_indent' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['tajaan_id_mesy'] = [
            'asc' => ['tajaan_mesy.id_mesy' => SORT_ASC],
            'desc' => ['tajaan_mesy.id_mesy' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['tajaan_id_syarikat'] = [
            'asc' => ['tajaan_mesy.id_syarikat' => SORT_ASC],
            'desc' => ['tajaan_mesy.id_syarikat' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['tajaan_id_agse_agsv'] = [
            'asc' => ['tajaan_mesy.id_agse_agsv' => SORT_ASC],
            'desc' => ['tajaan_mesy.id_agse_agsv' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['tajaan_no_kontrak'] = [
            'asc' => ['tajaan_mesy.no_kontrak' => SORT_ASC],
            'desc' => ['tajaan_mesy.no_kontrak' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['tajaan_edd_serah'] = [
            'asc' => ['tajaan_mesy.edd_serah' => SORT_ASC],
            'desc' => ['tajaan_mesy.edd_serah' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['tajaan_edd_terima'] = [
            'asc' => ['tajaan_mesy.edd_terima' => SORT_ASC],
            'desc' => ['tajaan_mesy.edd_terima' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['tajaan_jenis_tajaan'] = [
            'asc' => ['tajaan_mesy.jenis_tajaan' => SORT_ASC],
            'desc' => ['tajaan_mesy.jenis_tajaan' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['tajaan_harga'] = [
            'asc' => ['tajaan_mesy.harga' => SORT_ASC],
            'desc' => ['tajaan_mesy.harga' => SORT_DESC],
        ];
        //syarikat
        $dataProvider->sort->attributes['syarikat_nama_syarikat'] = [
            'asc' => ['maklumat_syarikat.nama_syarikat' => SORT_ASC],
            'desc' => ['maklumat_syarikat.nama_syarikat' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['syarikat_kod_bidang'] = [
            'asc' => ['maklumat_syarikat.kod_bidang' => SORT_ASC],
            'desc' => ['maklumat_syarikat.kod_bidang' => SORT_DESC],
        ];
        //kontrak
        $dataProvider->sort->attributes['kontrak_no_indent'] = [
            'asc' => ['butir_kontrak.no_indent' => SORT_ASC],
            'desc' => ['butir_kontrak.no_indent' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kontrak_no_kontrak'] = [
            'asc' => ['butir_kontrak.no_kontrak' => SORT_ASC],
            'desc' => ['butir_kontrak.no_kontrak' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kontrak_had_bumbung'] = [
            'asc' => ['butir_kontrak.had_bumbung' => SORT_ASC],
            'desc' => ['butir_kontrak.had_bumbung' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kontrak_tarikh_mula'] = [
            'asc' => ['butir_kontrak.tarikh_mula' => SORT_ASC],
            'desc' => ['butir_kontrak.tarikh_mula' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kontrak_tarikh_tamat_kontrak'] = [
            'asc' => ['butir_kontrak.tarikh_tamat_kontrak' => SORT_ASC],
            'desc' => ['butir_kontrak.tarikh_tamat_kontrak' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kontrak_revenue_kontrak'] = [
            'asc' => ['butir_kontrak.revenue_kontrak' => SORT_ASC],
            'desc' => ['butir_kontrak.revenue_kontrak' => SORT_DESC],
        ];
        //mesy
        $dataProvider->sort->attributes['mesy_tajuk_mesy'] = [
            'asc' => ['mesy_jawatan_kuasa.tajuk_mesy' => SORT_ASC],
            'desc' => ['mesy_jawatan_kuasa.tajuk_mesy' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['mesy_tarikh_mesy'] = [
            'asc' => ['mesy_jawatan_kuasa.tarikh_mesy' => SORT_ASC],
            'desc' => ['mesy_jawatan_kuasa.tarikh_mesy' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['mesy_id_lst_ahli'] = [
            'asc' => ['mesy_jawatan_kuasa.id_lst_ahli' => SORT_ASC],
            'desc' => ['mesy_jawatan_kuasa.id_lst_ahli' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['mesy_siri'] = [
            'asc' => ['mesy_jawatan_kuasa.siri' => SORT_ASC],
            'desc' => ['mesy_jawatan_kuasa.siri' => SORT_DESC],
        ];
        //kenderaan
        $dataProvider->sort->attributes['kenderaan_no_daftar'] = [
            'asc' => ['maklumat_agsv_agse.no_daftar' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.no_daftar' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kenderaan_url_gambar'] = [
            'asc' => ['maklumat_agsv_agse.url_gambar' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.url_gambar' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kenderaan_tarikh_masuk_khidmat'] = [
            'asc' => ['maklumat_agsv_agse.tarikh_masuk_khidmat' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.tarikh_masuk_khidmat' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['kenderaan_tarikh_serah_terima'] = [
            'asc' => ['maklumat_agsv_agse.tarikh_serah_terima' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.tarikh_serah_terima' => SORT_DESC],
        ];

//        $this->load($params,$ids,$siri);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
//        $this->tajaan_id_syarikat = $params;
        
        $query->andFilterWhere([

            'id' => $this->id,
            'no_indent' => $this->no_indent,
            'jumlah_indent' => $this->jumlah_indent,
            'baki_had_bumbumg' => $this->baki_had_bumbumg,
//            'siri' => $this->baki_had_bumbumg,
//            'nama_syarikat' => $this->$data,
        ]);
        
        
//        $this->mesy_siri = '1';
        
//        easy2/frontend/web/index.php?DataSearch%5Bmesy_siri%5D=1&DataSearch%5Bsyarikat_nama_syarikat%5D=AMDAC+SDN+BHD&DataSearch%5Bsyarikat_kod_bidang%5D=&DataSearch%5Bkenderaan_no_daftar%5D=&DataSearch%5Bkenderaan_url_gambar%5D=&DataSearch%5Btajaan_harga%5D=&DataSearch%5Bkenderaan_tarikh_masuk_khidmat%5D=&r=editor%2Findex&_pjax=%23dynagrid-1-pjax
        
        $query->andFilterWhere(['like', 'no_indent', $this->no_indent])
                ->andFilterWhere(['like', 'jenis_indent', $this->jenis_indent])
                ->andFilterWhere(['like', 'created_date', $this->created_date])
                ->andFilterWhere(['like', 'updated_date', $this->updated_date])

//                ->andFilterWhere(['like', 'jenis_tajaan', $this->jenis_tajaan])
                //kontrak
                ->andFilterWhere(['like', 'butir_kontrak.no_kontrak', $this->kontrak_no_kontrak])
                ->andFilterWhere(['like', 'butir_kontrak.revenue_kontrak', $this->kontrak_revenue_kontrak])
                ->andFilterWhere(['like', 'butir_kontrak.no_kontrak', $this->kontrak_no_kontrak])
                ->andFilterWhere(['like', 'butir_kontrak.revenue_kontrak', $this->kontrak_revenue_kontrak])
                //syarikat
                ->andFilterWhere(['like', 'maklumat_syarikat.kod_bidang', $this->syarikat_kod_bidang])
//                ->andFilterWhere(['like', 'maklumat_syarikat.nama_syarikat', 'AMDAC SDN BHD'])
                ->andFilterWhere(['like', 'maklumat_syarikat.nama_syarikat', $this->syarikat_nama_syarikat])
                ->andFilterWhere(['like', 'maklumat_syarikat.id', $this->syarikat_id])
                //mesy
                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.tajuk_mesy', $this->mesy_tajuk_mesy])
                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.tarikh_mesy', $this->mesy_tarikh_mesy])
                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.id_lst_ahli', $this->mesy_id_lst_ahli])
                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.siri', $this->mesy_siri])
//                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.siri', $siri])
                //kenderaan
                ->andFilterWhere(['like', 'maklumat_agsv_agse.no_daftar', $this->kenderaan_no_daftar])
                ->andFilterWhere(['like', 'maklumat_agsv_agse.url_gambar', $this->kenderaan_url_gambar])
                ->andFilterWhere(['like', 'maklumat_agsv_agse.tarikh_masuk_khidmat', $this->kenderaan_tarikh_masuk_khidmat])
                ->andFilterWhere(['like', 'maklumat_agsv_agse.tarikh_serah_terima', $this->kenderaan_tarikh_serah_terima])
        ;
//            ->andFilterWhere(['like', 'mesy_jawatan_kuasa.tajuk_mesy', $this->idMesy_tajuk])
//            ->andFilterWhere(['like', 'mesy_jawatan_kuasa.siri', $this->idMesy_siri])
//            ->andFilterWhere(['like', 'maklumat_agsv_agse.no_daftar', $this->idAgseAgsv_no_daftar]);
//            ->andFilterWhere(['like', 'maklumat_agsv_agse.no_daftar', $this->idAgseAgsv]);

        return $dataProvider;
    }

}
