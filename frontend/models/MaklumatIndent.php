<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

//use frontend\models\TajaanMesy;

/**
 * This is the model class for table "maklumat_indent".
 *
 * @property integer $id
 * @property string $no_indent
 * @property string $jenis_indent
 * @property double $jumlah_indent
 * @property double $baki_had_bumbumg
 * @property string $created_date
 * @property string $updated_date
 */
class MaklumatIndent extends ActiveRecord {

    public function behaviors() {
        return [

//            [
//                'class' => TimestampBehavior::className(),
//                'createdAtAttribute' => 'created_date',
//                'updatedAtAttribute' => 'updated_date',
//                'value' => date('Y-m-d H:i:s'),
//            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_date', 'updated_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_date'],
                ],
//                     'value' => date('Y-m-d H:i:s'),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'maklumat_indent';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['jumlah_indent', 'baki_had_bumbumg'], 'number'],
            [['created_date', 'updated_date'], 'safe'],
            [['no_indent', 'no_kontrak', 'jenis_indent'], 'string', 'max' => 50],
            ['no_indent', 'unique']
//            [['no_indent'], 'unique','on' => 'insert', 'message' => 'This indent no has already exist.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'no_indent' => 'No Indent',
            'no_kontrak' => 'No Kontrak',
            'jenis_indent' => 'Jenis Indent',
            'jumlah_indent' => 'Jumlah Indent',
            'baki_had_bumbumg' => 'Baki Had Bumbumg',
            'created_date' => 'Created Date',
            'updated_date' => 'Updated Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKontrak() {
        return $this->hasOne(ButirKontrak::className(), ['no_kontrak' => 'no_kontrak']);
    }

//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
    public function getSejarah() {
        return $this->hasOne(SejarahPembaikan::className(), ['no_indent' => 'no_indent']);
    }

    public function getTajaan() {
        return $this->hasOne(TajaanMesy::className(), ['no_indent' => 'no_indent']);
    }

    public function getSyarikat() {
        return $this->hasOne(MaklumatSyarikat::className(), ['id' => 'id_syarikat'])
                        ->via('kontrak');
//                        ->viaTable('butir_kontrak', ['no_indent' => 'no_indent']);
    }
    
    public function getMesy() {
        return $this->hasOne(MesyJawatanKuasa::className(), ['id' => 'id_mesy'])
//                        ->viaTable('tajaan_mesy', ['no_indent' => 'no_indent']);
                        ->via('tajaan');
    }
    
//    public function getMinit() {
//        return $this->hasOne(MinitMesy::className(), ['id_mesy' => 'id'])
//                        ->viaTable('mesy_jawatan_kuasa', ['id' => 'id'])
//                        ->viaTable('tajaan_mesy', ['no_indent' => 'no_indent']);
//    }
    
    public function getKenderaan() {
        return $this->hasOne(MaklumatAgsvAgse::className(), ['id' => 'id_agse_agsv'])
                        ->via('tajaan');
//                        ->viaTable('tajaan_mesy', ['no_indent' => 'no_indent']);
    }
}
