<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\data\ActiveDataProvider;
use frontend\models\TajaanMesy;

class AllSearch extends TajaanMesy {

    // add the public attributes that will be used to store the data to be search
//    public $tajuk_mesy;
//    public $siri;
//    public $id;
//    public $id_mesy;
//    public $jenis_tajaan;
    public $tajuk_mesy;
    public $no_daftar;

    // now set the rules to make those attributes safe
    public function rules() {
        return [
            // ... more stuff here
            //tajaan_mesy
//            [['id', 'id_mesy', 'id_syarikat', 'id_agse_agsv', 'qty'], 'integer'],
//            [['no_kontrak', 'jenis_tajaan', 'created_date', 'updated_date'], 'safe'],
//            [['harga_per_unit', 'harga'], 'number'],
//            [['id', 'id_mesy'], 'integer'],
//            [['jenis_tajaan', ], 'safe'],
            //mesy_jawatan_kuasa
//            [['id'], 'integer'],
//            [['tajuk_mesy', 'tarikh_mesy', 'id_lst_ahli', 'siri', 'created_date', 'updated_date'], 'safe'],
//            [['tajuk_mesy', 'siri'], 'safe'],
            [['tajuk_mesy', 'no_daftar'], 'string', 'max' => 50],
            [['tajuk_mesy', 'no_daftar'], 'safe'],
                //other table
                // ... more stuff here
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
//        $data = ['id', 'id_mesy','jenis_tajaan','tajuk_mesy', 'siri'];
        
//        $query = TajaanMesy::find();
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//        ]);
//        
        $data = ['idAgseAgsv', 'idMesy'];
        $query = TajaanMesy::find();
//        $query->joinWith($data);
        $query->joinWith('idAgseAgsv');
        $query->joinWith('idMesy');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        // Important: here is how we set up the sorting
        // The key is the attribute name on our "TourSearch" instance
        $dataProvider->sort->attributes['idAgseAgsv'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['maklumat_agsv_agse.no_daftar' => SORT_ASC],
            'desc' => ['maklumat_agsv_agse.no_daftar' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['idMesy'] = [
            'asc' => ['mesy_jawatan_kuasa.tajuk_mesy' => SORT_ASC],
            'desc' => ['mesy_jawatan_kuasa.tajuk_mesy' => SORT_DESC],
        ];
//        $this->load($params);
        // No search? Then return data Provider
//         if (!$this->validate()) {
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // We have to do some search... Lets do some magic
        $query->andFilterWhere([
                        //... other searched attributes here
            'id' => $this->id,
            'id_mesy' => $this->id_mesy,
            'id_syarikat' => $this->id_syarikat,
            'id_agse_agsv' => $this->id_agse_agsv,
            'qty' => $this->qty,
            'harga_per_unit' => $this->harga_per_unit,
            'harga' => $this->harga,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
                ])
                // Here we search the attributes of our relations using our previously configured
                // ones in "TourSearch"
                ->andFilterWhere(['like', 'maklumat_agsv_agse.no_daftar', $this->no_daftar])
                ->andFilterWhere(['like', 'mesy_jawatan_kuasa.tajuk_mesy', $this->tajuk_mesy]);

        return $dataProvider;
//        
//        $this->load($params);
//
//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }
//
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'id_mesy' => $this->id_mesy,
//            'id_syarikat' => $this->id_syarikat,
//            'id_agse_agsv' => $this->id_agse_agsv,
//            'qty' => $this->qty,
//            'harga_per_unit' => $this->harga_per_unit,
//            'harga' => $this->harga,
//            'created_date' => $this->created_date,
//            'updated_date' => $this->updated_date,
//            'idMesy.tajuk_mesy' => $this->tajuk_mesy,
//        ]);
//
//        $query->andFilterWhere(['like', 'no_kontrak', $this->no_kontrak])
//            ->andFilterWhere(['like', 'jenis_tajaan', $this->jenis_tajaan]);
//
//        return $dataProvider;
    }

}
