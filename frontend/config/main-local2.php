<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'VkzopkXwbyn9bLAtqyQORD0ZvwLl5le5',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';

    $config['modules']['gridview'] = [
        'class' => '\kartik\grid\Module',
    ];
    ////////////
    $config['modules']['datecontrol'] = [
        'class' => '\kartik\datecontrol\Module',
    ];
}

return $config;
