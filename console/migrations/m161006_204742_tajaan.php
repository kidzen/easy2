<?php

use yii\db\Migration;

class m161006_204742_tajaan extends Migration {

    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tajaan_mesy}}', [
            'id' => $this->primaryKey(),
            'no_indent' => $this->string(),
            'id_mesy' => $this->integer(),
            'id_syarikat' => $this->integer(),
            'id_agse_agsv' => $this->integer(),
            'no_kontrak' => $this->string(),
            'jenis_tajaan' => $this->string(),
            'senggaraan' => $this->text(),
            'harga' => $this->double(),
            'edd_terima' => $this->date(),
            'edd_serah' => $this->date(),
            'created_date' => $this->dateTime(),
            'updated_date' => $this->dateTime(),
                ], $tableOptions);
    }

    public function down() {
        $this->dropTable('{{%tajaan_mesy}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
