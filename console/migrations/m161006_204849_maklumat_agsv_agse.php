<?php

use yii\db\Migration;

class m161006_204849_maklumat_agsv_agse extends Migration {

  public function up() {
    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    }

    $this->createTable('{{%maklumat_agsv_agse}}', [
      'id' => $this->primaryKey(),
      'no_daftar' => $this->string()->unique(),
      'jenis_agsv_agse' => $this->string(),
      'url_gambar' => $this->string(),
      'tarikh_masuk_khidmat' => $this->date(),
      'tarikh_serah_terima' => $this->dateTime(),
      'created_date' => $this->dateTime(),
      'updated_date' => $this->dateTime(),
      ], $tableOptions);
    $this->populateData();
  }

  public function populateData() {
    $this->insert('{{%maklumat_agsv_agse}}', [
      'no_daftar' => 'ZZ 1234',
      'jenis_agsv_agse' => 'Trak 3 Tan',
      'url_gambar' => 'tiada_gambar.jpg',
      'tarikh_masuk_khidmat' => date('Y-m-d'),
      'tarikh_serah_terima' => date('Y-m-d'),
      'created_date' => date('Y-m-d H:i:s'),
      'updated_date' => date('Y-m-d H:i:s'),
      ], $tableOptions);
  }

  public function down() {
    $this->dropTable('{{%maklumat_agsv_agse}}');
  }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
    }
