<?php

use yii\db\Migration;

class m161006_204805_butir_kontrak extends Migration
{
   
    public function up() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

          $this->createTable('{{%butir_kontrak}}', [
            'id' => $this->primaryKey(),
            'no_indent' => $this->string(),
            'no_kontrak' => $this->string(),
            'id_syarikat' => $this->integer(),
            'had_bumbung' => $this->double(),
            'revenue_kontrak' => $this->double(),
            'tarikh_mula' => $this->date(),
            'tarikh_tamat_kontrak' => $this->date(),
            'created_date' => $this->dateTime(),
            'updated_date' => $this->dateTime(),
                ], $tableOptions);

    }

    public function down() {
        $this->dropTable('{{%butir_kontrak}}');
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
