<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201442_init extends Migration {

	public function up() {
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%user}}', [
			'id' => $this->primaryKey(),
			'username' => $this->string()->notNull()->unique(),
			'role' => $this->string()->notNull(),
			'auth_key' => $this->string(32)->notNull(),
			'password_hash' => $this->string()->notNull(),
			'password_reset_token' => $this->string()->unique(),
			'email' => $this->string()->notNull()->unique(),
			'status' => $this->smallInteger()->notNull()->defaultValue(1),
			'created_at' => $this->integer(),
			'updated_at' => $this->integer(),
			], $tableOptions);
       $this->execute($this->getUserSql());
	}


	private function getUserSql()
	{
		// $time = time();
		// $password_hash = Yii::$app->getSecurity()->generatePasswordHash("admin2");
		// $auth_key = Yii::$app->security->generateRandomString();
		return $this->insertTable('{{%user}}',[
			'username'=>'admin2',
			'email'=> 'admin2@gmail.com',
			'auth_key'=> Yii::$app->security->generateRandomString(),
			'password_hash'=> Yii::$app->security->generatePasswordHash('admin2'),
			'status'=> 1,
			'role'=> 'Administrator',
			'created_at'=> time(),
			'updated_at'=> time(),
			]);

		// return "INSERT INTO {{%user}} (`username`, `email`, `auth_key`, `password_hash`, `password_reset_token`, `role`, `status`, `created_at`, `updated_at`)
		// VALUES ('admin2', 'admin2@mail.com', '$auth_key', '$password_hash', '', 'Administrator', 1, $time, $time)";

	}
	public function down() {
		$this->dropTable('{{%user}}');
	}

}
