<?php

use yii\db\Migration;

class m161006_213044_data_migrate extends Migration {

    public function up() {

        foreach (array_filter(array_map('trim', explode(';', $this->getDataSql()))) as $query) {
            $this->execute($query);
        }
    }

    private function getDataSql() {
        $fileName = 'sistemgk4.sql';
        $myfile = fopen($fileName, "r") or die("Unable to open file!");
        $commandR = fread($myfile, filesize($fileName));
        fclose($myfile);

        $sql = $commandR;
        return "$sql";
    }

    public function down() {
//        $this->truncateTable('user');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
